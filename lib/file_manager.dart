import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pms_project/main.dart';

class MyFiles extends StatefulWidget {
  const MyFiles({super.key});

  @override
  State<MyFiles> createState() => _MyFilesState();
}

class _MyFilesState extends State<MyFiles> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              elevation: 0,
              backgroundColor: Colors.white,
              leading: Image.asset("assect/sort.png"),
              title: Text(
                'Hallo, Irfan!',
                style: TextStyle(
                  color: Color(0xFF505050),
                  fontSize: 18,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            SliverAppBar(
              toolbarHeight: 90,
              pinned: true,
              elevation: 0,
              backgroundColor: Colors.white,
              primary: false,
              title: Container(
                padding: EdgeInsets.all(6),
                decoration: BoxDecoration(
                    color: Color(0xFFF6F6F6),
                    borderRadius: BorderRadius.circular(90)),
                width: double.infinity,
                height: 62,
                child: TabBar(
                  unselectedLabelColor: Color(0xFF505050),
                  indicator: BoxDecoration(
                    color: Color(0xFF00A3FF),
                    borderRadius: BorderRadius.circular(90),
                  ),
                  tabs: [
                    Text(
                      'Recent',
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      'Storage',
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                      ),
                    )
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                decoration: BoxDecoration(
                    // color: Color(0xFFF6F6F6),
                ),
                height: MediaQuery.of(context).size.height,
                  child: TabBarView(
                children: [
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: 12,
                    itemBuilder: (context,index){
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            width: double.infinity,
                            height: 76,
                            decoration: ShapeDecoration(
                              color: Color(0x1900A3FF),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      width: 56,
                                      height: 55,
                                      decoration: ShapeDecoration(
                                        color: Color(0xFF00A3FF),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(15),
                                        ),
                                      ),
                                      child: Image.asset("assect/FolderFill.png"),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Documents',
                                          style: TextStyle(
                                            color: Colors.black.withOpacity(0.6000000238418579),
                                            fontSize: 18,
                                            fontFamily: 'Roboto',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          '17 Items',
                                          style: TextStyle(
                                            color: Colors.black.withOpacity(0.20999999344348907),
                                            fontSize: 10,
                                            fontFamily: 'Roboto',
                                            fontWeight: FontWeight.w700,
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),

                                Icon(Icons.arrow_forward_ios_rounded)
                              ],
                            ),
                          ),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>  HomePage()));
                          },
                        ),
                      );
                    },
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: 12,
                    itemBuilder: (context,index){
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          width: double.infinity,
                          height: 76,
                          decoration: ShapeDecoration(
                            color: Color(0x1900A3FF),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: 56,
                                    height: 55,
                                    decoration: ShapeDecoration(
                                      color: Color(0xFF00A3FF),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                    ),
                                    child: Image.asset("assect/FolderFill.png"),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Documents',
                                        style: TextStyle(
                                          color: Colors.black.withOpacity(0.6000000238418579),
                                          fontSize: 18,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        '17 Items',
                                        style: TextStyle(
                                          color: Colors.black.withOpacity(0.20999999344348907),
                                          fontSize: 10,
                                          fontFamily: 'Roboto',
                                          fontWeight: FontWeight.w700,
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),

                              Icon(Icons.arrow_forward_ios_rounded)
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              )),
            )
          ],
        ),
      ),
    );
  }
}
